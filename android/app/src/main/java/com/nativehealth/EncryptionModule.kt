package com.nativehealth

import android.os.Build
import android.util.Log
import com.facebook.react.bridge.*
import java.util.*

class EncryptionModule(reactContext: ReactApplicationContext?) : ReactContextBaseJavaModule(reactContext) {

    override fun getName(): String {
        return "Encryptor" // Name of the Native Modules.
    }

    //Custom function that we are going to export to JS
    @ReactMethod
    fun getDeviceName(cb: Callback) {
        try {
            cb.invoke(null, Build.MODEL)
        } catch (e: Exception) {
            cb.invoke(e.toString(), null)
        }
    }

    @ReactMethod
    fun encrypt(plainText: String, promise: Promise) {
        try {
            // Add your encryption logic here
            // (can use any JAVA encryption library or use default)
            val encryptedText = plainText + "This is encrypted text"
            promise.resolve(encryptedText) // return encryptedText
        } catch (e: Exception) {
            promise.reject("ENCRYPTION_FAILED", "Encryption Failed")
        }
    }

    @ReactMethod
    fun getAuthorizationAndReadData() {
        try {
            MainActivity().fitSignIn(FitActionRequestCode.READ_DATA)
        } catch (e: Exception) {
           Log.i("error", "error!!!!")
        }
    }

}