//
//  Controller.swift
//  nativehealth
//
//  Created by raksha on 19/1/2021.
//

import Foundation
import HealthKit
 
 // Swift Decorator to export Class to Objective-C
 // Declares Controller Class
@objc(Controller)
class Controller: NSObject {
  // Declares HealthKit store for our Project
 let healthStore = HKHealthStore()
  
  enum HealthDataType: String, Codable {
      case heartRate
      case bmi
      case bloodpressure
      case biologicalSex
      case stepCount
  }
 

  struct HealthDataItem: Codable {
      let endDate: Date
      let value: String
      let startDate: Date
      let type: HealthDataType
  }

  // Function Method to request Authorizations from Healthkit
 @objc
 func requestAuthorization() {
  if HKHealthStore.isHealthDataAvailable() {
              let readDataTypes : Set = [HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!,
                                         HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!,
                                         HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureDiastolic)!,
                                         HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureSystolic)!,
                                         HKObjectType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.biologicalSex)!,
                                         HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMassIndex)!,
                                         HKObjectType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.dateOfBirth)!]
              
              let writeDataTypes : Set = [HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!]
              
              healthStore.requestAuthorization(toShare: writeDataTypes, read: readDataTypes) { (success, error) in
                  if !success {
                      // Handle the error here.
                  }
              }
      }
 }
  
  @objc
  func sampleMethod(_ resolve: RCTPromiseResolveBlock, rejecter reject: RCTPromiseRejectBlock) {
    var biologicalSex: String? {
    if try! healthStore.biologicalSex().biologicalSex == HKBiologicalSex.female {
                print("You are female")
               return "Female"
            } else if try! healthStore.biologicalSex().biologicalSex == HKBiologicalSex.male {
                print("You are male")
              return "Male"
            } else if try! healthStore.biologicalSex().biologicalSex == HKBiologicalSex.other {
                print("You are not categorised as male or female")
              return nil
            }
          return nil
        }
        print("sex:", biologicalSex as Any)
        resolve(biologicalSex)
    }
  
  @objc
  func getSteps(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: RCTPromiseRejectBlock) {
    
    let type = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.day = 1

        let query = HKStatisticsCollectionQuery(quantityType: type,
                                               quantitySamplePredicate: nil,
                                               options: [.cumulativeSum],
                                               anchorDate: startOfDay,
                                               intervalComponents: interval)
    
            query.initialResultsHandler = { _, result, error in
                    var resultCount = 0.0
                    result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in

                    if let sum = statistics.sumQuantity() {
                        // Get steps (they are of double type)
                        resultCount = sum.doubleValue(for: HKUnit.count())
                    } // end if
                      print("result in xcode:", resultCount)
                      let stepCountItem = HealthDataItem(endDate: now, value: String(resultCount), startDate: startOfDay, type: .stepCount)
                              let healthData = [stepCountItem]
                              do {
                                let data = try JSONEncoder().encode(healthData)
                                let json = String(data: data, encoding: String.Encoding.utf8)
                                print("json data:", json as Any )
                                resolve(json)
                              } catch {
                                   //error handling
                              }
                }
            }
        healthStore.execute(query)
      }
  
  @objc
  func getBloodPressure(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: RCTPromiseRejectBlock) {
    var value : String = ""
    guard let type = HKQuantityType.correlationType(forIdentifier: HKCorrelationTypeIdentifier.bloodPressure),
                let systolicType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureSystolic),
                let diastolicType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureDiastolic) else {

                    return
            }
         let now = Date()
         let startOfDate = Calendar.current.startOfDay(for: now)
         let predicate = HKQuery.predicateForSamples(withStart: startOfDate, end: now, options: .strictStartDate)

            let sampleQuery = HKSampleQuery(sampleType: type, predicate: predicate, limit: 0, sortDescriptors: nil) { (sampleQuery, results, error) in
                if let dataList = results as? [HKCorrelation] {
                    for data in dataList
                    {
                        if let data1 = data.objects(for: systolicType).first as? HKQuantitySample,
                            let data2 = data.objects(for: diastolicType).first as? HKQuantitySample {

                            let value1 = data1.quantity.doubleValue(for: HKUnit.millimeterOfMercury())
                            let value2 = data2.quantity.doubleValue(for: HKUnit.millimeterOfMercury())
                            value = "\(value1)|\(value2)"
                            print("BP in xcode:", value)
                          let bloodPressureItem = HealthDataItem(endDate: now, value: value, startDate: startOfDate, type: .bloodpressure)
                                  let healthData = [bloodPressureItem]
                                  do {
                                    let data = try JSONEncoder().encode(healthData)
                                    let json = String(data: data, encoding: String.Encoding.utf8)
                                    print("json data:", json as Any )
                                    resolve(json)
                                  } catch {
                                       //error handling
                                  }
                        }
                    }
                }
            }
            healthStore.execute(sampleQuery)
      }
  
  @objc
  func getHeartRate(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: RCTPromiseRejectBlock) {
    
                    let calendar = NSCalendar.current
                    
                    let interval = NSDateComponents()
                    interval.day = 1

                     let endDate = Date()

                                          
                    guard let startDate = calendar.date(byAdding: .day, value: -1, to: endDate) else {
                        fatalError("*** Unable to calculate the start date ***")
                    }
                                        
                    let quantityType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)
     
                    let query = HKStatisticsCollectionQuery(quantityType: quantityType!,
                                                            quantitySamplePredicate: nil,
                                                                options: .discreteAverage,
                                                                anchorDate: startDate,
                                                                intervalComponents: interval as DateComponents)
                    
                    query.initialResultsHandler = {
                        query, results, error in
                        
                        guard let statsCollection = results else {
                         //error
                          return
                        }
                                            
                      statsCollection.enumerateStatistics(from: startDate, to: endDate) { statistics, stop in
                            if let quantity = statistics.averageQuantity() {
                                let value = quantity.doubleValue(for: HKUnit(from: "count/min"))
                                print("HeartRate in xcode:",value)
                      let heartRateItem = HealthDataItem(endDate: endDate, value: String(value), startDate: startDate, type: .heartRate)
                              let healthData = [heartRateItem]
                              do {
                                let data = try JSONEncoder().encode(healthData)
                                let json = String(data: data, encoding: String.Encoding.utf8)
                                print("json data:", json as Any )
                                resolve(json)
                              } catch {
                                   //error handling
                              }
                              
                            }
                        }
                        
                    }
            healthStore.execute(query)
      }

  @objc
  func getBMI(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: RCTPromiseRejectBlock) {
    let bodyMassType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMassIndex)
    let query = HKSampleQuery(sampleType: bodyMassType!, predicate: nil, limit: 1, sortDescriptors: nil) { (query, results, error) in
                if let result = results?.first as? HKQuantitySample {
                  let bodyMassIndex = result.quantity.doubleValue(for: HKUnit.count())
                    print("BMI in xcode",bodyMassIndex, result.endDate)
                  let bmiItem = HealthDataItem(endDate: result.endDate, value: String(bodyMassIndex), startDate: result.endDate, type: .bmi)
                          let healthData = [bmiItem]
                          do {
                            let data = try JSONEncoder().encode(healthData)
                            let json = String(data: data, encoding: String.Encoding.utf8)
                            print("json data:", json as Any )
                            resolve(json)
                          } catch {
                               //error handling
                          }
                    return
                }

            }
    
    
            healthStore.execute(query)
      }
  
//  @objc
//  func getNativeData(_ resolve: RCTPromiseResolveBlock, rejecter reject: RCTPromiseRejectBlock) {
//
//    var json : Any = {}
//
//    //Step count
//
//    let steptype = HKQuantityType.quantityType(forIdentifier: .stepCount)!
//        let now = Date()
//        let startOfDay = Calendar.current.startOfDay(for: now)
//        var interval = DateComponents()
//        interval.day = 1
//
//        let stepquery = HKStatisticsCollectionQuery(quantityType: steptype,
//                                               quantitySamplePredicate: nil,
//                                               options: [.cumulativeSum],
//                                               anchorDate: startOfDay,
//                                               intervalComponents: interval)
//
//         stepquery.initialResultsHandler = { _, result, error in
//                    var resultCount = 0.0
//                    result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
//
//                    if let sum = statistics.sumQuantity() {
//                        // Get steps (they are of double type)
//                        resultCount = sum.doubleValue(for: HKUnit.count())
//                    } // end if
//                      print("result in xcode:", resultCount)
//                      let stepCountItem = HealthDataItem(endDate: now, value: resultCount, startDate: startOfDay, type: .stepCount)
//                      let healthData = [stepCountItem]
//                      do {
//                        let data = try JSONEncoder().encode(healthData)
//                        json = String(data: data, encoding: String.Encoding.utf8) as Any
//                        print("json data:", json as Any )
//                      } catch {
//                           //error handling
//                      }
//                }
//            }
//        healthStore.execute(stepquery)
//
//
//  // Blood pressure
//
//    var value : Any = ""
//    guard let bptype = HKQuantityType.correlationType(forIdentifier: HKCorrelationTypeIdentifier.bloodPressure),
//                let systolicType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureSystolic),
//                let diastolicType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureDiastolic) else {
//
//                    return
//            }
//         let startDate = Calendar.current.startOfDay(for: now)
//         let endDate = now
//         let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: .strictStartDate)
//
//            let sampleQuery = HKSampleQuery(sampleType: bptype, predicate: predicate, limit: 0, sortDescriptors: nil) { (sampleQuery, results, error) in
//                if let dataList = results as? [HKCorrelation] {
//                    for data in dataList
//                    {
//                        if let data1 = data.objects(for: systolicType).first as? HKQuantitySample,
//                            let data2 = data.objects(for: diastolicType).first as? HKQuantitySample {
//
//                            let value1 = data1.quantity.doubleValue(for: HKUnit.millimeterOfMercury())
//                            let value2 = data2.quantity.doubleValue(for: HKUnit.millimeterOfMercury())
//                            value = "\(value1) / \(value2)"
//                            print("BP in xcode:", value)
//                          let bloodPressureItem = HealthDataItem(endDate: now, value: value1, startDate: startOfDay, type: .bloodpressure)
//                          let healthData = [bloodPressureItem]
//                          do {
//                            let data = try JSONEncoder().encode(healthData)
//                            json = String(data: data, encoding: String.Encoding.utf8) as Any
//                            print("json data:", json as Any )
//                          } catch {
//                               //error handling
//                          }
//                        }
//                    }
//                }
//            }
//            healthStore.execute(sampleQuery)
//
//
//    // Heart Rate
//
//                    let calendar = NSCalendar.current
//                    let hrinterval = NSDateComponents()
//                    hrinterval.day = 1
//                     let hrendDate = Date()
//
//
//                    guard let hrstartDate = calendar.date(byAdding: .day, value: -1, to: hrendDate) else {
//                        fatalError("*** Unable to calculate the start date ***")
//                    }
//
//                    let quantityType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)
//
//                    let hrquery = HKStatisticsCollectionQuery(quantityType: quantityType!,
//                                                            quantitySamplePredicate: nil,
//                                                                options: .discreteAverage,
//                                                                anchorDate: hrstartDate,
//                                                                intervalComponents: hrinterval as DateComponents)
//
//              hrquery.initialResultsHandler = {
//                        query, results, error in
//
//                        guard let statsCollection = results else {
//                         //error
//                          return
//                        }
//
//                      statsCollection.enumerateStatistics(from: hrstartDate, to: hrendDate) { statistics, stop in
//                            if let quantity = statistics.averageQuantity() {
//                                let value = quantity.doubleValue(for: HKUnit(from: "count/min"))
//                                print("HeartRate in xcode:",value)
//                      let heartRateItem = HealthDataItem(endDate: hrendDate, value: value, startDate: hrstartDate, type: .heartRate)
//                              let healthData = [heartRateItem]
//                              do {
//                                let data = try JSONEncoder().encode(healthData)
//                                json = String(data: data, encoding: String.Encoding.utf8) as Any
//                                print("json data:", json as Any )
//                              } catch {
//                                   //error handling
//                              }
//
//                            }
//                        }
//
//                    }
//            healthStore.execute(hrquery)
//
//    // BMI
//
//    let bodyMassType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMassIndex)
//    let bmiquery = HKSampleQuery(sampleType: bodyMassType!, predicate: nil, limit: 1, sortDescriptors: nil) { (query, results, error) in
//                if let result = results?.first as? HKQuantitySample {
//                  let bodyMassIndex = result.quantity.doubleValue(for: HKUnit.count())
//                    print("BMI in xcode",bodyMassIndex, result.endDate)
//                  let bmiItem = HealthDataItem(endDate: result.endDate, value: bodyMassIndex, startDate: result.startDate, type: .bmi)
//                          let healthData = [bmiItem]
//                          do {
//                            let data = try JSONEncoder().encode(healthData)
//                            json = String(data: data, encoding: String.Encoding.utf8) as Any
//                            print("json data:", json as Any )
//                          } catch {
//                               //error handling
//                          }
//                    return
//                }
//            }
//            healthStore.execute(bmiquery)
//
//    print("json data:", json as Any )
//    resolve(json)
//    }
//
//
  
  
  @objc
 static func requiresMainQueueSetup() -> Bool {
   return true
 }
}

