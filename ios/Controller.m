//
//  Controller.m
//  nativehealth
//
//  Created by raksha on 19/1/2021.
//


#import "React/RCTBridgeModule.h"
#import <React/RCTLog.h>


// This exports our Controller class in Controller.swift
@interface RCT_EXTERN_MODULE(Controller, NSObject)

// Exports our requestAuthorization method in the class
RCT_EXTERN_METHOD(requestAuthorization)

RCT_EXTERN_METHOD(sampleMethod: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)

RCT_EXTERN_METHOD(getSteps: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)

RCT_EXTERN_METHOD(getBloodPressure: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)

RCT_EXTERN_METHOD(getHeartRate: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)

RCT_EXTERN_METHOD(getNativeData: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)

RCT_EXTERN_METHOD(getBMI: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)

@end
