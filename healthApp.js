import React, {useState} from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native';
import {HealthkitController, encrypt, decrypt} from './HealthKit.js';
import { NativeEventEmitter, NativeModules } from 'react-native';
import { postFhirData } from './fhir.js';
import jsonFile from './mixed.json';

const isIOS = Platform.OS === 'ios';
const isAndroid = Platform.OS === 'android';


export default function healthApp() {
  const [name, setName] = useState('john');
  const [sex, setSex] = useState('Nil');
  const [systolic, setSystolic] = useState('0');
  const [diastolic, setDiastolic] = useState('0');
  const [step, setStep] = useState('0');
  const [heartRate, setHearRate] = useState('0');
  const [bmi, setBMI] = useState('0');


  clickHandlerBp = async() => {
    const log = await HealthkitController.getBloodPressure()
    .then(result => {
      let res = JSON.parse(result)
      for (let i = 0; i < res.length; i++) {
        let final = res[i];
        let finalValue = final.value.toString()
        let sys = parseInt(finalValue.substring(0, finalValue.indexOf("|")), 10);
        let dia = parseInt(finalValue.substring(finalValue.indexOf("|") + 1), 10);
        let systolic = sys.toString()
        let diastolic = dia.toString()
        setSystolic(systolic)
        setDiastolic(diastolic)
    }});
  }

  clickHandlerStep = async() => {
    const log = await HealthkitController.getSteps()
    .then(result => {
      let res = JSON.parse(result)
      for (let i = 0; i < res.length; i++) {
        let final = res[i];
        final = parseInt(final.value, 10)
        final = final.toString()
        setStep(final)
    }});
  }

  clickHandlerHearRate = async() => {
    const log = await HealthkitController.getHeartRate()
    .then(result => {
      let res = JSON.parse(result)
      for (let i = 0; i < res.length; i++) {
        let final = res[i];
        final = parseInt(final.value, 10)
        final = final.toString()
        setHearRate(final)
    }});
  }

  clickHandlerBMI = async() => {
    const log = await HealthkitController.getBMI()
    .then(result => {
      let res = JSON.parse(result)
      for (let i = 0; i < res.length; i++) {
        let final = res[i];
        final = parseInt(final.value, 10)
        final = final.toString()
        setBMI(final)
    }});
  }

  // clickHandlerSex = async() => {
  //   const log = await HealthkitController.sampleMethod()
  //   .then(result => {
  //     // let res = result.toString()
  //     console.warn("Sex:", result)
  //     // setSex(res)
  //   });

  // }

  click = async() => {
    await HealthkitController.requestAuthorization()
    clickHandlerHearRate()
    clickHandlerBMI()
    clickHandlerStep()
    clickHandlerBp()
    // NativeModules.Encryptor.getAuthorizationAndReadData();
    // const decryptText = await decrypt('encrypted text');
  }

  post = async() => {
    console.log(JSON.stringify(jsonFile).split("{value}").join(100))
    let success = await postFhirData()
  }


  return (
    <View style={styles.container}>
        <View style={{ paddingBottom: 20 }}></View>
        <View style={styles.rowContainer}>
        <TouchableOpacity style={styles.text1} onPress={click}>
    <Text style={styles.label}>Request Health Data</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.text1} onPress={post}>
        <Text style={styles.label}>POST to fhir</Text>
      </TouchableOpacity>
        </View>
      <View style={{ paddingBottom: 50 }}></View>
      <View style={{flexDirection: "row", alignContent: "space-between", alignItems: "center"}}>
      <Text style={styles.label}>Name</Text>
      <TextInput value={name} placeholder='Type Here...'  style={styles.text} textAlign='center'/>
      </View>
      <View style={{ paddingBottom: 50 }}></View>
      <View style={{flexDirection: "row", alignContent: "space-between"}}>
        <View style={{flexDirection: "row", alignContent: "space-between", alignItems: "center"}}>
      <Text style={styles.label}>Systolic</Text>
      <TextInput value={systolic} placeholder='Type Here...' style={styles.text} textAlign='center'/>   
      </View>
      <View style={{ paddingRight: 20 }}></View>
      <View style={{flexDirection: "row", alignContent: "space-between", alignItems: "center"}}>
      <Text style={styles.label}>Diastolic</Text>
      <TextInput value={diastolic} placeholder='Type Here...' style={styles.text} textAlign='center'/> 
      </View> 
       </View>
      <View style={{ paddingBottom: 50 }}></View>
      <View style={{flexDirection: "row", alignContent: "space-between", alignItems: "center"}}>
      <Text style={styles.label}>StepCount</Text>
      <TextInput value={step} placeholder='Type Here...' style={styles.text} textAlign='center'/>
      </View>
      <View style={{ paddingBottom: 50 }}></View>
      <View style={{flexDirection: "row", alignContent: "space-between", alignItems: "center"}}>
      <Text style={styles.label}>HearRate</Text>
      <TextInput value={heartRate} placeholder='Type Here...'  style={styles.text} textAlign='center'/>
      </View>
      <View style={{ paddingBottom: 50 }}></View>
      <View style={{flexDirection: "row", alignContent: "space-between", alignItems: "center"}}>
      <Text style={styles.label}>BMI</Text>
      <TextInput value={bmi} placeholder='Type Here...'  style={styles.text} textAlign='center'/>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#EAEDED',
    alignItems: 'center',
    justifyContent: 'center',
  },
  rowContainer: {
    flexDirection: 'row',
   justifyContent: "flex-start"
  },
  touch: {
    backgroundColor: 'grey',
    padding: 15
  },
  text1:{
    borderRightWidth: 2,
    borderLeftWidth: 2,
    borderTopWidth:2,
    borderBottomWidth: 2,
    backgroundColor: "#D0D3D4",
    height: 40,
    width:193,
    borderColor: 'black',
    justifyContent: "center",
    alignItems: "center",
    fontSize: 16,
    paddingRight: 10,
    paddingLeft: 10,
    marginLeft:4,
    marginRight: 4
  },
  text: {
   borderRadius: 5, 
    borderRightWidth: 2,
    borderLeftWidth: 2,
    borderTopWidth:2,
    borderBottomWidth: 2,
    backgroundColor: "#D0D3D4",
    height: 30,
    width: 100,
    borderColor: 'black',
    justifyContent: "center",
    alignItems: "center",
    fontSize: 16
  },
  label: {
    fontFamily: 'helvetica',
    paddingRight:10,
    fontSize: 18,
    alignItems: "center"
  }
});