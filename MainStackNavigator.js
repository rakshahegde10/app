
import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import Home from './Home'
import healthApp from './healthApp'

const Stack = createStackNavigator()

function MainStackNavigator() {
  return (
    <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen
        name='Home'
        component={Home}
        options={{ title: 'Home Screen' }}
      />
      <Stack.Screen
        name='healthApp'
        component={healthApp}
        options={{ title: 'healthApp' }}
      />
    </Stack.Navigator>
  </NavigationContainer>
  )
}

export default MainStackNavigator